<?php 
/**
* Save a file in a field if field it's empty
*/

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\file\Entity\File;

/**
 * Implements hook_entity_presave().
 */
function hook_entity_presave(Drupal\Core\Entity\EntityInterface $entity) {

  switch ($entity->bundle()) {

    case 'my_entity':
      // Setting the title with the value of field_date.
      
      $field = $entity->get('field_section_image');
      $parent = $entity->getParentEntity();

      global $base_url;
      $theme = \Drupal::theme()->getActiveTheme();
      $uri = $base_url.'/'. $theme->getPath() .'/img/trainstationdefault.jpg';

     
      // check first if the file exists for the uri
      $files = \Drupal::entityTypeManager()
        ->getStorage('file')
        ->loadByProperties(['uri' => $uri]);
      $file = reset($files);

      // if not create a file
      if (!$file) {
        $file = File::create([
          'uri' => $uri,
        ]);
        $file->save();
      }

      $entity->field_section_image[] = [
        'target_id' => $file->id(),
        'alt' => 'Alt text',
        'title' => 'Title',
      ];
        
     break;
  }
}