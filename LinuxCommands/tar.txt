Compress an Entire Directory or a Single File
tar -czvf name-of-archive.tar.gz /path/to/directory-or-file

Extract an Archive
tar -xzvf archive.tar.gz